package com.homework.web4.board.mapper;

import com.homework.web4.board.vo.BoardData;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface BoardMapper {

    int getArticleCount() throws Exception;

    List<BoardData.ArticleDesc> getArticleList(Map<String, Object> params) throws Exception;

    BoardData.ArticleInfo getArticleDetail(Map<String, Object> params) throws Exception;

    int writeArticle(Map<String, Object> params) throws Exception;

    int deleteArticle(Map<String, Object> params) throws Exception;

    int increaseArticleCount(Map<String, Object> params) throws Exception;

    int updateArticle(Map<String, Object> params) throws Exception;
}
