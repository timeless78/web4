package com.homework.web4.board.service;

import com.homework.web4.board.common.Uploader;
import com.homework.web4.board.mapper.BoardMapper;
import com.homework.web4.board.vo.BoardData;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class BoardService {

    @Value("${server.file.storage.path}")
    private String serverPath;

    private final BoardMapper mapper;

    public int getArticleCount() throws Exception {
        return mapper.getArticleCount();
    }

    public List<BoardData.ArticleDesc> getArticleDescList(Map<String, Object> params) throws Exception {
        return mapper.getArticleList(params);
    }

    public int createArticle(BoardData.ArticleRequest request) throws Exception {
        Uploader.setServerPath(serverPath);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("boardTitle", request.getBoardTitle());
        params.put("boardContents", request.getBoardContents());
        params.put("boardWriter", request.getBoardWriter());

        String originFilename = request.getFile().getOriginalFilename();
        String storedFilename = "";
        boolean hasFile = originFilename != null && !request.getFile().isEmpty();
        if (hasFile) {
            storedFilename = Uploader.retrieveStoredFilename(request.getFile());
            params.put("originFileName", originFilename);
            params.put("storedFileName", storedFilename);
        }

        int result = mapper.writeArticle(params);
        if (result > 0 && hasFile) {
            Uploader.uploadFile(request.getFile(), storedFilename);
        }
        return result;
    }

    public BoardData.ArticleInfo getArticleInfo(String boardId) throws Exception {

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("boardId", boardId);

        BoardData.ArticleInfo article = mapper.getArticleDetail(params);

        mapper.increaseArticleCount(params);
        article.setBoardCount(article.getBoardCount() + 1);

        return article;
    }

    public int deleteArticle(String boardId) throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("boardId", boardId);

        BoardData.ArticleInfo article = mapper.getArticleDetail(params);
        int result = mapper.deleteArticle(params);
        if (result > 0) {
            String existFile = article.getStoredFileName();
            Uploader.deleteFile(existFile);
        }

        return result;
    }

    public int modifyArticle(BoardData.ArticleRequest article) {
        Uploader.setServerPath(serverPath);

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("boardId", article.getBoardId());
        params.put("boardTitle", article.getBoardTitle());
        params.put("boardContents", article.getBoardContents());
        params.put("boardWriter", article.getBoardWriter());

        String storedFilename = Uploader.retrieveStoredFilename(article.getFile());
        params.put("originFileName", article.getFile().getOriginalFilename());
        params.put("storedFileName", storedFilename);

        int result = 0;
        try {
            // 기존의 파일 삭제 기능 보류, 일단 그냥 업데이트만 진행

            result = mapper.updateArticle(params);
            if (result > 0) {
                Uploader.uploadFile(article.getFile(), storedFilename);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Transactional
    public int deleteSelectedArticle(String boardIds) throws Exception{
        int result = 0;

        Map<String, Object> params = new HashMap<String, Object>();
        String[] ids = boardIds.split(",");

        for(String boardId: ids) {
            params.put("boardId", boardId);

            BoardData.ArticleInfo article = mapper.getArticleDetail(params);

            result = mapper.deleteArticle(params);
            if (result > 0) {
                if (article.getStoredFileName() != null) {
                    Uploader.deleteFile(article.getStoredFileName());
                }
            } else {
                throw new Exception("file delete boardId : " + boardId);
            }
        }
        return result;
    }
}
