package com.homework.web4.board.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.UUID;

@Repository
public class Uploader {

    @Value("${server.file.storage.path}")
    private static String filePath;

    public static String retrieveStoredFilename(MultipartFile file) {

        if (file == null || file.isEmpty()) {
            return null;
        }

        String originFilename = file.getOriginalFilename();
        String originExt = originFilename.substring(originFilename.lastIndexOf(".")+1);

        String uuid = UUID.randomUUID().toString();
        String storedFilename = uuid.replace("-", "").substring(0, 16) + "." + originExt;
        return storedFilename;
    }

    public static boolean uploadFile(MultipartFile file, String storedFilename) throws Exception {

        String storedFullPath = filePath + storedFilename;
        if (storedFullPath.length() < 1) {
            return false;
        }

        File newFile = new File(storedFullPath);
        if(!newFile.getParentFile().exists()) {
            newFile.getParentFile().mkdirs();
        }

        newFile.createNewFile();
        file.transferTo(newFile);

        return true;
    }

    public static void deleteFile(String filename) throws Exception {
        String fullPath = filePath + filename;

        File existFile = new File(fullPath);
        if (existFile.exists()) {
            existFile.delete();
        }
    }

    public static String getServerPath() {
        return filePath;
    }
    public static void setServerPath(String serverPath) {
        filePath = serverPath;
    }
}
