package com.homework.web4.board.common;


public class Pagination {

    private int totalPage;
    private int nowPageNumber;
    private int blockPerPage;
    private int pagePerRows;
    private int totalRows;
    private int nowBlock;
    private int totalBlock;

    public Pagination(int nowPageNumber, int totalRows, int ppr) {
        // 로직에서는 페이지번호 시작을 0 부터로 처리
        this.nowPageNumber = nowPageNumber - 1;
        this.totalRows = totalRows;
        this.pagePerRows = ppr;
        this.blockPerPage = 10; // 한 블럭당 페이지 10개씩 표현
    }

    public int getBeginPage() {
        return this.nowPageNumber * this.pagePerRows;
    }

    public int getEndPage() {
        return this.pagePerRows;
    }

    public int getTotalPage() {
        double result = Math.ceil((double)this.totalRows / this.pagePerRows);
        return (int)result;
    }

    public int getNowBlock() {
        double retVal = ((double)this.nowPageNumber) / this.blockPerPage;
        return (int)(retVal);	// 현재 블럭 = 현재 페이지 / 블럭당 페이지 수
    }

    public int getTotalBlock(int totalPage) {
        double retVal = Math.ceil((double)totalPage / this.blockPerPage);
        return (int)(retVal);
    }

    public String pageHTML() {
        this.totalPage = this.getTotalPage();
        this.nowBlock = this.getNowBlock();
        this.totalBlock = this.getTotalBlock(totalPage);
        int pageNumber = 0;
        // html을 담을 빌
        StringBuilder sb = new StringBuilder();

        // 만약 블럭이 넘어간게 있을 경우
        if (nowBlock > 0) {
            pageNumber = (nowBlock - 1) * blockPerPage;
            sb.append("<li class='page-item'>");
            sb.append("  <a class='page-link' href='javascript:void(0);' onclick='goPage("+pageNumber+");'>");
            sb.append("처음</a>");
            sb.append("</li>");
        }

        if (nowPageNumber > 0) {
            pageNumber = 0;
            sb.append("<li class='page-item'>");
            sb.append("  <a class='page-link' href='javascript:void(0);' onclick='goPage("+pageNumber+");'>");
            sb.append("이전</a>");
            sb.append("</li>");
        }

        // 페이지 그리기
        for(int i = 0; i < blockPerPage; i++) {
            pageNumber = (nowBlock * blockPerPage) + i;

            sb.append("<li class='page-item" + (pageNumber == nowPageNumber ? " active" : "") + "'>");
            sb.append("  <a class='page-link' href='javascript:void(0);' onclick='goPage("+pageNumber+");'>");
            sb.append((pageNumber+1) + "</a>");
            sb.append("</li>");

            // 만약 한블럭당 10개의 페이지 번호를 그리는데
            // 실제 페이지는 3개만 필요한 경우 중간에 루프를 중지
            if ((pageNumber+1) == totalPage) {
                break;
            }
        }

        // 다음블럭으로 가기 버튼 만들기
        if((nowBlock) + 1 < this.totalBlock) {
            pageNumber = (nowBlock+1) * blockPerPage;
            sb.append("<li class='page-item'>");
            sb.append("  <a class='page-link' href='javascript:void(0);' onclick='goPage("+pageNumber+");'>");
            sb.append("다음</a>");
            sb.append("</li>");
        }

        // 마지막 페이지로 이동 버튼 만들기
        if ((nowPageNumber + 1) < totalPage) {
            sb.append("  <a class='page-link' href='javascript:void(0);' onclick='goPage("+(totalPage -1)+");'>");
            sb.append("마지막</a>");
            sb.append("</li>");
        }

        return sb.toString();
    }
}