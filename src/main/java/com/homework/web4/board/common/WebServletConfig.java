package com.homework.web4.board.common;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;

@Configuration
public class WebServletConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor( new LoginCheckInterceptor() )
                .addPathPatterns("/board")
                .addPathPatterns("/board/write")
                .addPathPatterns("/board/detail")
                .addPathPatterns("/board/modify")
                .addPathPatterns("/board/delete")
                .addPathPatterns("/board/delete/selected")
                .addPathPatterns("/board/down");
    }

    @Bean
    Filter characterEncodingFilter() {
        CharacterEncodingFilter encoding = new CharacterEncodingFilter();
        encoding.setEncoding("utf-8");
        encoding.setForceEncoding(true);
        return encoding;
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(DataSize.ofMegabytes(100));
        factory.setMaxRequestSize(DataSize.of(100, DataUnit.MEGABYTES));
        return factory.createMultipartConfig();
    }

    @Bean
    MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }
}
