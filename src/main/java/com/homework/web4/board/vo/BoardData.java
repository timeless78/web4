package com.homework.web4.board.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

public class BoardData {

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ArticleInfo {
        private int boardId;
        private String boardTitle;
        private String boardContents;
        private String boardWriter;
        private int boardCount;
        private String updateDate;
        private String createDate;
        private String storedFileName;
        private String originFileName;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ArticleDesc {
        private int boardId;
        private String boardTitle;
        private String boardWriter;
        private int boardCount;
        private String createDate;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ArticleRequest {
        private int boardId;
        private String boardTitle;
        private String boardContents;
        private String boardWriter;
        private MultipartFile file;
    }
}
