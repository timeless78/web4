package com.homework.web4.board.vo;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class BoardStore {

    private Map<String, Object> boardMap;

    public BoardStore(){
        this.boardMap = new HashMap<>();
    }

    public void addObject(String key, Object value) {
        Object existObj = this.boardMap.get(key);
        if (existObj != null) {
            this.boardMap.remove(key);
        }
        this.boardMap.put(key, value);
    }

    public Object getObject(String key) {
        return this.boardMap.get(key);
    }
}
