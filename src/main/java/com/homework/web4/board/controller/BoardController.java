package com.homework.web4.board.controller;

import com.homework.web4.board.common.Pagination;
import com.homework.web4.board.service.BoardService;
import com.homework.web4.board.vo.BoardData;
import com.homework.web4.login.vo.UserData;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class BoardController {

    private final BoardService service;

    @Value("${server.file.storage.path}")
    private String serverPath;

    @GetMapping("/board")
    public ModelAndView displayBoard(@RequestParam(name="page", defaultValue = "1", required = false) int page,
                                     HttpServletRequest request) {
        ModelAndView view = new ModelAndView();
        view.setViewName("views/board/board_list_page");

        try {
            Map<String, Object> params = new HashMap<>();

            int pagePerRows = 10;
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("pagePerRows")) {
                        pagePerRows = Integer.parseInt(cookie.getValue());
                    }
                }
            }

            int totalCount = service.getArticleCount();

            Pagination paging = new Pagination(page, totalCount, pagePerRows);

            params.put("start", paging.getBeginPage());
            params.put("count", paging.getEndPage());

            List<BoardData.ArticleDesc> articles = service.getArticleDescList(params);

            view.addObject("pagePerRows", pagePerRows);
            view.addObject("boardCount", totalCount);
            view.addObject("boardList", articles);
            view.addObject("pageHTML", paging.pageHTML());

        } catch(Exception e) {
            e.printStackTrace();
        }


        return view;
    }

    @GetMapping("/board/pagePerRows")
    @ResponseBody
    public String invalidatePagePerRows(@RequestParam(value = "pagePerRows") int pagePerRows,
                                        HttpServletResponse response) {
        Cookie cookie = new Cookie("pagePerRows", String.valueOf(pagePerRows));
        cookie.setMaxAge(60*60*24);
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
        return "200";
    }

    @GetMapping("/board/write")
    public ModelAndView displayWritePage() {
        return new ModelAndView("views/board/board_create_page");
    }

    @GetMapping("/board/detail")
    public ModelAndView displayDetailPage(@RequestParam(value="boardId") String boardId) {
        ModelAndView view = new ModelAndView("views/board/board_detail_page");

        try {

            BoardData.ArticleInfo info = service.getArticleInfo(boardId);
            if (info != null) {
                view.addObject("articleInfo", info);
            }

        } catch(Exception e) {
            e.printStackTrace();

        }

        return view;
    }

    @GetMapping("/board/modify")
    public ModelAndView displayModifyPage(@RequestParam(value="boardId") String boardId) {
        ModelAndView view = new ModelAndView("views/board/board_modify_page");

        try {
            BoardData.ArticleInfo info = service.getArticleInfo(boardId);
            if (info != null) {
                view.addObject("articleInfo", info);
            }

        } catch(Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @PostMapping("/board/write")
    @ResponseBody
    public Map<String, Object> createArticle(@ModelAttribute BoardData.ArticleRequest article) {
        Map<String, Object> resultMap = new HashMap<>();

        try {

            int result = service.createArticle(article);
            if (result > 0) {
                resultMap.put("resultCode", 200);
            } else {
                throw new Exception("게시글 쓰기 실패");
            }

        } catch(Exception e) {
            e.printStackTrace();
            resultMap.put("resultCode", 500);
        }

        return resultMap;
    }

    @GetMapping("/board/delete")
    public ModelAndView deleteArticle(@RequestParam(name="boardId") String boardId, HttpServletRequest request) {
        ModelAndView view = new ModelAndView();

        UserData.UserInfo info = (UserData.UserInfo)request.getSession().getAttribute("userInfo");
        if (info != null) {
//            String sessionUser = info.getUserName();

            try {
//                BoardData.ArticleInfo article = service.getArticleInfo(boardId);

                // 이름을 비교 해서 글쓴이만 삭제 하려고 했으나, 편의상 그냥 지우는 로직으로 변경
                int result = service.deleteArticle(boardId);
                if (result > 0) {
                    view.setViewName("redirect:/board");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return view;
    }

    @PostMapping("/board/modify")
    @ResponseBody
    public Map<String, Object> modifyArticle(@ModelAttribute BoardData.ArticleRequest article) {
        Map<String, Object> resultMap = new HashMap<>();

        try {

            int result = service.modifyArticle(article);
            if (result > 0) {
                resultMap.put("resultCode", 200);
            } else {
                throw new Exception("게시글 수정 실패");
            }

        } catch(Exception e) {
            e.printStackTrace();
            resultMap.put("resultCode", 500);
        }

        return resultMap;
    }

    @PostMapping("/board/delete/selected")
    @ResponseBody
    public Map<String, Object> deleteListArticle(@RequestParam(name="boardIds") String boardIds) {
        Map<String, Object> resultMap = new HashMap<>();

        try {
            int result = service.deleteSelectedArticle(boardIds);

            if (result > 0) {
                resultMap.put("resultCode", 200);
            } else {
                throw new Exception("삭제 실패");
            }

        } catch(Exception e) {
            e.printStackTrace();
            resultMap.put("resultCode", 500);
        }

        return resultMap;
    }

    @GetMapping("/board/down")
    @ResponseBody
    public ResponseEntity<UrlResource> downloadFiles(@RequestParam(name="boardId") String boardId){

        String originFileName = null;
        String storedFileName = null;
        HttpHeaders header = new HttpHeaders();
        UrlResource res = null;

        try {
            BoardData.ArticleInfo article = service.getArticleInfo(boardId);

            storedFileName = article.getStoredFileName();
            originFileName = article.getOriginFileName();

            String fullPath = serverPath + storedFileName;

            File file = new File(fullPath);
            if (file != null && file.exists()) {

                String mimeType = Files.probeContentType(Paths.get(file.getAbsolutePath()));
                if (mimeType == null) {
                    mimeType="octet-stream";	 // 일반 이진파일 타입
                }

                res = new UrlResource(file.toURI());

                //한글깨짐 방지
                String encordedFilename = URLEncoder.encode(originFileName, "utf-8").replace("+", "%20");
                header.set("content-Disposition", "attachment;filename=" + encordedFilename + ";filename*= UTF-8''" + encordedFilename);
                header.setCacheControl("no-cache");
                header.setContentType(MediaType.parseMediaType(mimeType));

            } else {
                throw new Exception("파일이 존재하지 않습니다.");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<UrlResource>(res, header, HttpStatus.OK);
    }
}
