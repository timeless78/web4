package com.homework.web4.signup.controller;

import com.homework.web4.signup.service.SignupService;
import com.homework.web4.signup.vo.SignupData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class SignupController {

    private final SignupService service;

    @GetMapping("/signup")
    public ModelAndView displaySignup() {
        return new ModelAndView("views/signup/signup_page");
    }

    public String padLeft(String s, int n) {
        return String.format("%0" + n + "d", Integer.parseInt(s));
    }

    @PostMapping("/signup")
    @ResponseBody
    public Map<String, Object> signup(@ModelAttribute SignupData.SignupRequest signupReq) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        // 바로 UserData 객체를 생성(builder로)해서 파라메터로 넘길 수 있으나 생성의 주체를 service로 넘겨서 처리 합니다.
        Map<String, String> params = new HashMap<>();
        params.put("userEmail", signupReq.getEmailInput());
        params.put("userPasswd", signupReq.getPasswordInput());
        params.put("userName", signupReq.getUserNameInput());
        params.put("userGender", signupReq.getGenderSelect());

        // 날자가 유효한지 계산을 해야 하나 이 부분은 생략 합니다. ( 윤달, 월별 말일 날자 )
        String month = padLeft(signupReq.getBirthMonthSelect(), 2);
        String day = padLeft(signupReq.getBirthDaySelect(), 2);
        String userBirth = String.format("%s%s%s", signupReq.getBirthYearInput(), month, day);
        params.put("userBirth", userBirth);

        try {

            if (service.createUser(params)) {
                resultMap.put("resultCode", 200);
            } else {
                throw new Exception("User creation failed");
            }

        } catch (Exception e) {
            e.printStackTrace();
            resultMap.put("resultCode", 500);
        }

        return resultMap;
    }
}
