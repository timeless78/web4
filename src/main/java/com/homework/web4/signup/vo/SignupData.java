package com.homework.web4.signup.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

public class SignupData {

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SignupRequest {
        private String emailInput;
        private String passwordInput;
        private String passwordConfirm;
        private String userNameInput;
        private String birthYearInput;
        private String birthMonthSelect;
        private String birthDaySelect;
        private String genderSelect;
        private boolean agreement;
    }
}
