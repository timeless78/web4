package com.homework.web4.signup.mapper;

import com.homework.web4.login.vo.UserData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SignupMapper {

    int addUserAccount(UserData.UserAccount account) throws Exception;

    int addUserInfo(UserData.UserInfo info) throws Exception;
}
