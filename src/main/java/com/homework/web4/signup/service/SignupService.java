package com.homework.web4.signup.service;

import com.homework.web4.login.vo.UserData;
import com.homework.web4.signup.mapper.SignupMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SignupService {

    private final SignupMapper mapper;

    public boolean createUser(Map<String, String> params) throws Exception {

        String userUUID = UUID.randomUUID().toString();

        UserData.UserAccount newAccount = UserData.UserAccount
                .builder()
                .userUid(userUUID)
                .userEmail(params.get("userEmail"))
                .userPasswd(params.get("userPasswd"))
                .build();

        int ret = mapper.addUserAccount(newAccount);
        if (ret > 0) {
            UserData.UserInfo newInfo = UserData.UserInfo
                    .builder()
                    .userUid(userUUID)
                    .userName(params.get("userName"))
                    .userGender(params.get("userGender"))
                    .userBirth(Integer.parseInt(params.get("userBirth")))
                    .build();

            ret = mapper.addUserInfo(newInfo);
        }

        return ret > 0;
    }
}
