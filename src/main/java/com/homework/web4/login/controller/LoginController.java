package com.homework.web4.login.controller;

import com.homework.web4.board.common.ZRsaSecurity;
import com.homework.web4.login.service.LoginService;
import com.homework.web4.login.vo.UserData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;

    @GetMapping("/login")
    public ModelAndView displayLogin(HttpServletRequest request)  {
        ModelAndView view = new ModelAndView();
        view.setViewName("/views/login/login_page");
        return view;
    }

    @GetMapping("/login/error")
    public ModelAndView displayLoginError(HttpServletRequest request)  {
        ModelAndView view = new ModelAndView();
        view.setViewName("/views/common/error");
        return view;
    }

    @PostMapping("/login")
    @ResponseBody
    public Map<String, Object> login(@RequestParam(name="userEmail") String securedUserEmail,
                                     @RequestParam(name="userPasswd") String securedUserPasswd,
                                     HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        try {
            // RSA 객체 선언
            ZRsaSecurity security = new ZRsaSecurity();

            PrivateKey privateKey = (PrivateKey)request.getSession().getAttribute("_rsaPrivateKey_");

            // 암호화된 아이디와 패스워드 복호화
            String userEmail = security.decryptRSA(privateKey, securedUserEmail);
            String userPasswd = security.decryptRSA(privateKey, securedUserPasswd);

            UserData.UserAccount userAccount = loginService.doLogin(userEmail, userPasswd);
            if (userAccount != null) {
                // 사용한 비공개키 제거
                request.getSession().removeAttribute("_rsaPrivateKey_");

                UserData.UserInfo info = loginService.getUserInfo(userEmail);
                request.getSession().setAttribute("userInfo", info);

                resultMap.put("resultCode", 200);
            } else {
                throw new Exception("유저를 찾을 수 없습니다.");
            }

        } catch (Exception e) {
            resultMap.put("resultCode", 500);
            e.printStackTrace();

        }

        return resultMap;
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpServletRequest request)  {
        HttpSession session = request.getSession();
        session.invalidate();

        return new ModelAndView("redirect:/index");
    }

    @GetMapping("/rsa/key")
    @ResponseBody
    public Map<String, Object> getRsaKeyPair(HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<>();

        try {

            ZRsaSecurity security = new ZRsaSecurity();
            // 비공개키 객체 생성
            PrivateKey privateKey = security.getPrivateKey();

            // 기존에 저장된 비공개키를 지우고 새로 만들어 저장
            if (request.getSession().getAttribute("_rsaPrivateKey_") != null) {
                request.getSession().removeAttribute("_rsaPrivateKey_");
            }

            // 세션에 저장
            request.getSession().setAttribute("_rsaPrivateKey_", privateKey);

            // id, passwd 변경할 공개키 저장
            String publicKeyModules = security.getRsaPublicKeyModulus();
            String publicKeyExponent = security.getRsaPublicKeyExponent();

            // 공개키를 map에 저장해서 리턴
            resultMap.put("publicKeyModules", publicKeyModules);
            resultMap.put("publicKeyExponent", publicKeyExponent);

        } catch(Exception e) {
            e.printStackTrace();
        }

        return resultMap;
    }
}
