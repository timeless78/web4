package com.homework.web4.login.service;

import com.homework.web4.login.mapper.LoginMapper;
import com.homework.web4.login.vo.UserData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class LoginService {

    private final LoginMapper loginMapper;

    public UserData.UserAccount doLogin(String email, String password) throws Exception {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userEmail", email);
        params.put("userPassword", password);

        return loginMapper.getUserAccount(params);
    }

    public UserData.UserInfo getUserInfo(String email) throws Exception {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("userEmail", email);

        return loginMapper.getUserInfo(params);
    }
}
