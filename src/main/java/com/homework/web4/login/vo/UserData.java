package com.homework.web4.login.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


public class UserData {

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UserInfo {
        private String userUid;
        private String userName;
        private String userGender;
        private int userBirth;
        private String createTime;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UserAccount {
        private String userUid;
        private String userEmail;
        private String userPasswd;
        private String userLoginTime;
        private String userAuthType;
    }

}
