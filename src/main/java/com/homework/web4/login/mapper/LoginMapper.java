package com.homework.web4.login.mapper;

import com.homework.web4.login.vo.UserData;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface LoginMapper {

    UserData.UserAccount getUserAccount(Map<String, Object> param) throws Exception;
    UserData.UserInfo getUserInfo(Map<String, Object> param) throws Exception;

}
