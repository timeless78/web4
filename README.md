# homework-web4



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/timeless78/web4.git
git branch -M main
git push -uf origin main
```

## replace your 'application.properties' file

```
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
spring.datasource.url=jdbc:mariadb://localhost:3306/homework
spring.datasource.username=root
spring.datasource.password=1234

server.file.storage.path=/Users/timeless/Documents/Workspace/ITKorea/Web4/storage/downloads
```
Please!! Modify the above settings according to your PC environment.
1) db port number
2) db username
3) db password
4) server.file.storage.path

## build database on your test pc

- [ ] [This is sample query]
```
/* db 생성 */
CREATE DATABASE homework;
USE homework;
show databases;

/* table 생성 - 유저 정보 테이블 */
DROP TABLE homework.tbl_user;
CREATE TABLE homework.tbl_user(
	user_uid varchar(100) not null,
	user_name varchar(100) not null,
	user_gender varchar(50) not null,
	user_birth int not null,
	create_time dateTime default now(),
	primary key(user_uid)
);

SELECT * FROM tbl_user;

INSERT into tbl_user (user_uid, user_name, user_gender, user_birth)
			values("UUID1fad388e-e92d-4425-a169-426e2e2d0376", "hongkil", "man", 20230407);
			

/* table 생성 - 유저 계정 정보 테이블 */
DROP TABLE tbl_account;
CREATE TABLE tbl_account(
	user_uid varchar(100) not null,
	user_email varchar(100) not null,
	user_passwd varchar(100) not null,
	user_login_time dateTime default now(),
	user_auth_type varchar(10) default 'USER', 
	primary key(user_uid)
);

SELECT * from tbl_account;

INSERT into tbl_account (user_uid, user_email, user_passwd)
			values("UUID1fad388e-e92d-4425-a169-426e2e2d0376", "admin@web4homework.com", "1234");
			
/* table 생성 - board 정보 테이블 */
CREATE TABLE tbl_board (
  board_id int not null auto_increment,
  board_title varchar(255) not null,
  board_contents text default '',
  board_writer varchar(255) not null,
  board_count int default 0,
  create_date datetime default current_timestamp,
  update_date datetime default current_timestamp,
  primary key(board_id)
) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

SELECT * FROM tbl_board;

INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물1", "이 게시물은 테스트1 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물2", "이 게시물은 테스트2 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물3", "이 게시물은 테스트3 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물4", "이 게시물은 테스트4 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물5", "이 게시물은 테스트5 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물6", "이 게시물은 테스트6 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물7", "이 게시물은 테스트7 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물8", "이 게시물은 테스트8 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물9", "이 게시물은 테스트9 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물10", "이 게시물은 테스트10 게시물 입니다.", "hongkil" );

INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물11", "이 게시물은 테스트11 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물12", "이 게시물은 테스트12 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물13", "이 게시물은 테스트13 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물14", "이 게시물은 테스트14 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물15", "이 게시물은 테스트15 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물16", "이 게시물은 테스트16 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물17", "이 게시물은 테스트17 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물18", "이 게시물은 테스트18 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물19", "이 게시물은 테스트19 게시물 입니다.", "hongkil" );
INSERT into tbl_board (board_title, board_contents, board_writer)
			values("테스트 게시물20", "이 게시물은 테스트20 게시물 입니다.", "hongkil" );


ALTER table tbl_board add column stored_file_name varchar(255);
ALTER table tbl_board add column origin_file_name varchar(255);


```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/timeless78/web4/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
